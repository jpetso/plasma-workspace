# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# Shinjo Park <kde@peremen.name>, 2009, 2014, 2019, 2021, 2022, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-01 01:39+0000\n"
"PO-Revision-Date: 2023-02-28 23:50+0100\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 21.12.3\n"

#: menu.cpp:100
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Show KRunner"
msgstr "KRunner 표시"

#: menu.cpp:105
#, kde-format
msgid "Open Terminal"
msgstr "터미널 열기"

#: menu.cpp:109
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Lock Screen"
msgstr "화면 잠그기"

#: menu.cpp:118
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Leave…"
msgstr "떠나기…"

#: menu.cpp:127
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Configure Display Settings…"
msgstr "디스플레이 설정..."

#: menu.cpp:283
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Configure Contextual Menu Plugin"
msgstr "콘텍스트 메뉴 플러그인 설정하기"

#: menu.cpp:293
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "[Other Actions]"
msgstr "[기타 동작]"

#: menu.cpp:296
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "Wallpaper Actions"
msgstr "배경 그림 동작"

#: menu.cpp:300
#, kde-format
msgctxt "plasma_containmentactions_contextmenu"
msgid "[Separator]"
msgstr "[구분선]"

#~ msgctxt "plasma_containmentactions_contextmenu"
#~ msgid "Run Command..."
#~ msgstr "명령 실행..."
