# translation of plasma_applet_org.kde.image.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2014, 2015, 2017.
# Mthw <jari_45@hotmail.com>, 2018, 2019.
# SPDX-FileCopyrightText: 2019, 2020, 2021, 2023 Matej Mrenica <matejm98mthw@gmail.com>
# Ferdinand Galko <galko.ferdinand@gmail.com>, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma_applet_org.kde.image\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-12-19 01:35+0000\n"
"PO-Revision-Date: 2023-11-11 15:13+0100\n"
"Last-Translator: Matej Mrenica <matejm98mthw@gmail.com>\n"
"Language-Team: Slovak <kde-i18n-doc@kde.org>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Lokalize 23.08.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Roman Paholík"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "wizzardsk@gmail.com"

#: imagepackage/contents/ui/AddFileDialog.qml:57
#, kde-format
msgctxt "@title:window"
msgid "Open Image"
msgstr "Otvoriť obrázok"

#: imagepackage/contents/ui/AddFileDialog.qml:69
#, kde-format
msgctxt "@title:window"
msgid "Directory with the wallpaper to show slides from"
msgstr "Adresár s tapetou na zobrazenie snímok"

#: imagepackage/contents/ui/config.qml:126
#, kde-format
msgid "Positioning:"
msgstr "Umiestnenie:"

#: imagepackage/contents/ui/config.qml:129
#, kde-format
msgid "Scaled and Cropped"
msgstr "Zväčšený a orezaný"

#: imagepackage/contents/ui/config.qml:133
#, kde-format
msgid "Scaled"
msgstr "Zväčšený"

#: imagepackage/contents/ui/config.qml:137
#, kde-format
msgid "Scaled, Keep Proportions"
msgstr "Zväčšený, zachovať proporcie"

#: imagepackage/contents/ui/config.qml:141
#, kde-format
msgid "Centered"
msgstr "Na stred"

#: imagepackage/contents/ui/config.qml:145
#, kde-format
msgid "Tiled"
msgstr "Vydláždiť"

#: imagepackage/contents/ui/config.qml:173
#, kde-format
msgid "Background:"
msgstr "Farba pozadia:"

#: imagepackage/contents/ui/config.qml:174
#, kde-format
msgid "Blur"
msgstr "Rozmazanie"

#: imagepackage/contents/ui/config.qml:183
#, kde-format
msgid "Solid color"
msgstr "Plná farba"

#: imagepackage/contents/ui/config.qml:194
#, kde-format
msgid "Select Background Color"
msgstr "Výber farby pozadia"

#: imagepackage/contents/ui/main.qml:34
#, kde-format
msgid "Open Wallpaper Image"
msgstr "Otvoriť obrázok tapety"

#: imagepackage/contents/ui/main.qml:40
#, kde-format
msgid "Next Wallpaper Image"
msgstr "Nasledujúci obrázok tapety"

#: imagepackage/contents/ui/ThumbnailsComponent.qml:70
#, kde-format
msgid "Images"
msgstr "Obrázky"

#: imagepackage/contents/ui/ThumbnailsComponent.qml:74
#, kde-format
msgctxt "@action:button the thing being added is an image file"
msgid "Add…"
msgstr "Pridať…"

#: imagepackage/contents/ui/ThumbnailsComponent.qml:80
#, kde-format
msgctxt "@action:button the new things being gotten are wallpapers"
msgid "Get New…"
msgstr "Získať nové..."

#: imagepackage/contents/ui/WallpaperDelegate.qml:31
#, kde-format
msgid "Open Containing Folder"
msgstr "Otvoriť priečinok s obsahom"

#: imagepackage/contents/ui/WallpaperDelegate.qml:37
#, kde-format
msgid "Restore wallpaper"
msgstr "Obnoviť tapetu"

#: imagepackage/contents/ui/WallpaperDelegate.qml:42
#, kde-format
msgid "Remove Wallpaper"
msgstr "Odstrániť tapetu"

#: plasma-apply-wallpaperimage.cpp:29
#, kde-format
msgid ""
"This tool allows you to set an image as the wallpaper for the Plasma session."
msgstr "Tento nástroj umožňuje nastaviť obrázok ako tapetu pre Plasma sedenie."

#: plasma-apply-wallpaperimage.cpp:31
#, kde-format
msgid ""
"An image file or an installed wallpaper kpackage that you wish to set as the "
"wallpaper for your Plasma session"
msgstr ""
"Obrazový súbor alebo nainštalovaný balík tapiet, ktorý chcete nastaviť ako "
"tapetu pre svoje Plasma sedenie"

#: plasma-apply-wallpaperimage.cpp:45
#, kde-format
msgid ""
"There is a stray single quote in the filename of this wallpaper (') - please "
"contact the author of the wallpaper to fix this, or rename the file "
"yourself: %1"
msgstr ""
"V názve tejto tapety (') sa nachádza zblúdená jednoduchá úvodzovka - "
"kontaktujte prosím autora tapety, aby to napravil, alebo súbor premenujte "
"sami: %1"

#: plasma-apply-wallpaperimage.cpp:85
#, kde-format
msgid "An error occurred while attempting to set the Plasma wallpaper:\n"
msgstr "Pri pokuse o nastavenie Plasma tapety sa vyskytla chyba:\n"

#: plasma-apply-wallpaperimage.cpp:89
#, kde-format
msgid ""
"Successfully set the wallpaper for all desktops to the KPackage based %1"
msgstr "Úspešne nastavená tapeta pre všetky plochy na KPackage na základe %1"

#: plasma-apply-wallpaperimage.cpp:91
#, kde-format
msgid "Successfully set the wallpaper for all desktops to the image %1"
msgstr "Úspešne nastavená tapeta pre všetky desktopy na obrázok %1"

#: plasma-apply-wallpaperimage.cpp:97
#, kde-format
msgid ""
"The file passed to be set as wallpaper does not exist, or we cannot identify "
"it as a wallpaper: %1"
msgstr ""
"Súbor odovzdaný na nastavenie ako tapety neexistuje, alebo ho nemôžeme "
"identifikovať ako tapetu: %1"

#. i18n people, this isn't a "word puzzle". there is a specific string format for QFileDialog::setNameFilters
#: plugin/imagebackend.cpp:336
#, kde-format
msgid "Image Files"
msgstr "Súbory obrázkov"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:48
#, kde-format
msgid "Order:"
msgstr "Poradie:"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:55
#, kde-format
msgid "Random"
msgstr "Náhodné"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:59
#, kde-format
msgid "A to Z"
msgstr "A po Z"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:63
#, kde-format
msgid "Z to A"
msgstr "Z po A"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:67
#, kde-format
msgid "Date modified (newest first)"
msgstr "Dátum zmeny (najnovšie ako prvé)"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:71
#, kde-format
msgid "Date modified (oldest first)"
msgstr "Dátum zmeny (nastaršie ako prvé)"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:96
#, kde-format
msgid "Group by folders"
msgstr "Zoskupiť podľa priečinkov"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:108
#, kde-format
msgid "Change every:"
msgstr "Zmeniť každých:"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:118
#, kde-format
msgid "%1 hour"
msgid_plural "%1 hours"
msgstr[0] "%1 hodina"
msgstr[1] "%1 hodiny"
msgstr[2] "%1 hodín"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:138
#, kde-format
msgid "%1 minute"
msgid_plural "%1 minutes"
msgstr[0] "%1 minúta"
msgstr[1] "%1 minúty"
msgstr[2] "%1 minút"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:158
#, kde-format
msgid "%1 second"
msgid_plural "%1 seconds"
msgstr[0] "%1 sekunda"
msgstr[1] "%1 sekundy"
msgstr[2] "%1 sekúnd"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:189
#, kde-format
msgid "Folders"
msgstr "Priečinky"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:193
#, kde-format
msgctxt "@action button the thing being added is a folder"
msgid "Add…"
msgstr "Pridať…"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:231
#, kde-format
msgid "Remove Folder"
msgstr "Odstrániť priečinok"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:242
#, kde-format
msgid "Open Folder…"
msgstr "Otvoriť priečinok…"

#: slideshowpackage/contents/ui/SlideshowComponent.qml:257
#, kde-format
msgid "There are no wallpaper locations configured"
msgstr "Nie sú nastavené žiadne umiestnenia tapiet"

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:49
#, kde-format
msgctxt "@action:inmenu"
msgid "Set as Wallpaper"
msgstr "Nastaviť ako tapetu"

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:52
#, kde-format
msgctxt "@action:inmenu Set as Desktop Wallpaper"
msgid "Desktop"
msgstr "Pracovná plocha"

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:58
#, kde-format
msgctxt "@action:inmenu Set as Lockscreen Wallpaper"
msgid "Lockscreen"
msgstr "Uzamknutá obrazovka"

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:64
#, kde-format
msgctxt "@action:inmenu Set as both lockscreen and Desktop Wallpaper"
msgid "Both"
msgstr "Obidve"

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:97
#, kde-kuit-format
msgctxt "@info %1 is the dbus error message"
msgid "An error occurred while attempting to set the Plasma wallpaper:<nl/>%1"
msgstr "Pri pokuse o nastavenie Plasma tapety sa vyskytla chyba:<nl/>%1"

#: wallpaperfileitemactionplugin/wallpaperfileitemaction.cpp:110
#, kde-format
msgid "An error occurred while attempting to open kscreenlockerrc config file."
msgstr ""
"Pri pokuse o otvorenie konfiguračného súboru kscreenlockerrc sa vyskytla "
"chyba."

#~ msgid "Add Image…"
#~ msgstr "Pridať obrázok..."

#~ msgid "Add Folder…"
#~ msgstr "Pridať priečinok..."

#~ msgid "Recommended wallpaper file"
#~ msgstr "Odporúčaný súbor tapety"

#~ msgid "There are no wallpapers in this slideshow"
#~ msgstr "V tejto prezentácií nie sú žiadne pozadia"

#~ msgid "Add Custom Wallpaper"
#~ msgstr "Pridať vlastnú tapetu"

#~ msgid "Remove wallpaper"
#~ msgstr "Odstrániť tapetu"

#~ msgid "%1 by %2"
#~ msgstr "%1 od %2"

#, fuzzy
#~| msgid "Remove Wallpaper"
#~ msgid "Wallpapers"
#~ msgstr "Odstrániť tapetu"

#~ msgctxt "<image> by <author>"
#~ msgid "By %1"
#~ msgstr "Od %1"

#~ msgid "Download Wallpapers"
#~ msgstr "Stiahnuť tapety"

#~ msgid "Hours"
#~ msgstr "Hodiny"

#~ msgid "Use blur background filling"
#~ msgstr "Použiť rozmazanú výplň pozadia"

#~ msgid "Open..."
#~ msgstr "Otvoriť..."

#~ msgctxt "Unknown Author"
#~ msgid "Unknown"
#~ msgstr "Neznáme"

#~ msgid "Image Files (*.png *.jpg *.jpeg *.bmp *.svg *.svgz *.xcf)"
#~ msgstr "Súbory obrázkov (*.png *.jpg *.jpeg *.bmp *.svg *.svgz *.xcf)"

#~ msgid "Screenshot"
#~ msgstr "Snímka obrazovky"

#~ msgid "Preview"
#~ msgstr "Náhľad"
